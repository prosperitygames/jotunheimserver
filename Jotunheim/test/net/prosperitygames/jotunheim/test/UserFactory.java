package net.prosperitygames.jotunheim.test;

import net.prosperitygames.entities.User;

public class UserFactory {
	public static User correct() {
		User user = new User();
		
		user.setId(0L);
		user.setName("John Doe");
		user.setEmail("johndoe@example.com");
		user.setLogin("johndoe");
		user.setPassword("johndoe");
		user.setConfirmedEmail(true);
		
		return user;
	}
	
	public static User anotherCorrect() {
		User user = new User();
		
		user.setId(1L);
		user.setName("Jane Poe");
		user.setEmail("janepoe@example.com");
		user.setLogin("janepoe");
		user.setPassword("janepoe");
		user.setConfirmedEmail(true);
		
		return user;
	}

	public static User oneMoreCorrect() {
		User user = new User();
		
		user.setId(2L);
		user.setName("Bart Simpson");
		user.setEmail("bart@example.com");
		user.setLogin("bart");
		user.setPassword("simpson");
		user.setConfirmedEmail(true);
		
		return user;
	}
	
	public static User invalidEmail() {
		User user = correct();
		user.setEmail("bla");
		return user;
	}
}
