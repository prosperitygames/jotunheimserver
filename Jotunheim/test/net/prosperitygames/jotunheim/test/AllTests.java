package net.prosperitygames.jotunheim.test;

import net.prosperitygames.jotunheim.logic.fight.TestBattleLogic;
import net.prosperitygames.jotunheim.logic.fight.TestInMenoryFightManager;
import net.prosperitygames.jotunheim.logic.login.TestInMemoryAuthentication;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({TestInMenoryFightManager.class, 
	           TestInMemoryAuthentication.class,
	           TestBattleLogic.class})
public class AllTests {
}
