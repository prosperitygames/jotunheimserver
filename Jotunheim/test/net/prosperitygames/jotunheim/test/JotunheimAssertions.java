package net.prosperitygames.jotunheim.test;

import java.util.Collection;

import org.junit.Assert;

public class JotunheimAssertions {
	public static void assertIsEmpty(Collection<?> collection) {
		assertIsEmpty(null, collection);
	}
	
	public static void assertIsEmpty(String message, Collection<?> collection) {
		if (!collection.isEmpty()) {
			Assert.fail(message);
		}
	}
	
	public static void assertIsNotEmpty(Collection<?> collection) {
		assertIsNotEmpty(null, collection);
	}
	
	public static void assertIsNotEmpty(String message, Collection<?> collection) {
		if (collection.isEmpty()) {
			Assert.fail(message);
		}
	}
	
}
