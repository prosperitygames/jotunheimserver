package net.prosperitygames.jotunheim.logic.fight;

import static org.junit.Assert.*;
import net.prosperitygames.entities.Robot;
import net.prosperitygames.entities.RobotPart;
import net.prosperitygames.entities.RobotPartType;
import net.prosperitygames.entities.Skill;
import net.prosperitygames.jotunheim.random.MockRandomNumberGenerator;
import net.prosperitygames.jotunheim.util.RobotFactory;

import org.junit.Before;
import org.junit.Test;

public class TestBattleLogic {
	private static final double MAX_ERROR = Math.pow(10, -4);
	
	private BattleLogic logic;
	private MockRandomNumberGenerator randomGenerator;
	private Robot attacker;
	private Robot defender;
	private RobotPart attackerLeftArm;
	private RobotPart attackerChest;
	private RobotPart defenderHead;
	private RobotPart defenderLeftArm;
	private Skill attackerChasingHand;
	private Skill attackerStoneMissiles;
	
	@Before
	public void setUp() throws Exception {
		logic = new BattleLogic();
		randomGenerator = new MockRandomNumberGenerator();
		logic.setRandomNumberGenerator(randomGenerator);
		
		attacker = RobotFactory.loadRobot("beowulf.xml");
		defender = RobotFactory.loadRobot("beowulf.xml");
		attackerLeftArm = attacker.getPart(RobotPartType.LEFT_ARM);
		attackerChest   = attacker.getPart(RobotPartType.CHEST);
		defenderHead    = defender.getPart(RobotPartType.HEAD);
		defenderLeftArm = defender.getPart(RobotPartType.LEFT_ARM);
		attackerChasingHand   = attackerLeftArm.getSkill();
		attackerStoneMissiles = attackerChest.getSkill();
	}
	
	@Test
	public void itShouldCalculateThePhysicalDamageForASimpleAttack() throws Exception {
		/*
		 * Here we are testing the physical damage inflicted by the chasing
		 * hand skill over the defender's head.
		 * 
		 * The chasing hand - That's the skill the attacker is using:
		 *    - It is a physical attack with power of 40;
		 *    - Its accuracy if 100%;
		 *    - Its kind is Light.
		 * 
		 * In order to calculate the damage of the attack we'll use the
		 * following formula:
		 * 
		 * DAMAGE = [(2 x Level + 10) / 250 x (Attack / Defense) x (Skill Base Power)] + 2 x (Type Influence) 
		 * 
		 * The variable values are:
		 *    - Level = 100
		 *    - Attack = 110
		 *    - Defense = 95
		 *    - Skill Base Power = 40
		 *    - Type Influence = NONE
		 * 
		 * So, our calculus is solved as following:
		 * 
		 * DAMAGE = [(2 x 100 + 10) / 250 x (110 / 95) x (40)] + 2
		 * DAMAGE = 40.9052
		 */
		assertEquals(40.9052, logic.calculatePhysicalDamage(attacker, defender, attackerChasingHand, defenderHead), MAX_ERROR);
	}
	
	@Test
	public void itShouldCalculateThePhysicalDamageForSkillsWithDifferentTypes() throws Exception {
		/*
		 * DAMAGE = [(2 x Level + 10) / 250 x (Attack / Defense) x (Skill Base Power)] + 2 x (Type Influence)
		 * 
		 * The variable values are:
		 *    - Level = 100
		 *    - Attack = 110
		 *    - Defense = 95
		 *    - Skill Base Power = 40
		 *    - Type Influence = 0.5
		 * 
		 * DAMAGE = [(2 x 100 + 10) / 250 x (110 / 95) x (40)] + 2 * 0.5
		 * DAMAGE = 39.9052
		 */
		assertEquals(39.9052, logic.calculatePhysicalDamage(attacker, defender, attackerChasingHand, defenderLeftArm), MAX_ERROR);
	}
	
	@Test
	public void itShouldConsiderAllSkillsToCalculateTheTotalDamage() {
		/**
		 * In this test we consider the influence of all skils of attacker and defender.
		 * In this case we have two: the chasing hand and the power blessing.
		 * Power blessing - Both robots are using this skill on its heads:
		 *    - It increases the power of every attack in 30%;
		 *    - It decreases 10% of the robot's life (enery) in every round;
		 * So, the total damage must be the original damage increased by 30%:
		 * DAMAGE = 39.9052
		 * TOTAL DAMAGE = 39.9052 x 1.3 = 51.87676 
		 */
		assertEquals(51.87676, logic.calculateTotalPhysicalDamage(attacker, defender, attackerChasingHand, defenderLeftArm), MAX_ERROR);
	}
	
	@Test
	public void itShouldCalculateZeroForAMissedAttackDamage() {
		setupRandomGeneratorForAMiss(attackerStoneMissiles);
		assertEquals(0, logic.calculatePhysicalDamage(attacker, defender, attackerStoneMissiles, defenderHead), MAX_ERROR);
	}
	
	@Test
	public void itShouldCalculateTheDamageForACriticalAttack() {
		/*
		 * DAMAGE = [(2 x Level + 10) / 250 x (Attack / Defense) x (Skill Base Power)] + 2 x (Type Influence) x (Critical Bonus)
		 * 
		 * The variable values are:
		 *    - Level = 100
		 *    - Attack = 110
		 *    - Defense = 95
		 *    - Skill Base Power = 100
		 *    - Type Influence = 1.0
		 *    - Critical Bonus = 2 if its a critical attack or else 1 
		 * 
		 * DAMAGE = [(2 x 100 + 10) / 250 x (110 / 95) x (40)] + (2 * 1.0 * 2)
		 * DAMAGE = 101.2631
		 */
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		double damage = logic.calculatePhysicalDamage(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		assertEquals(101.2631, damage, MAX_ERROR);
	}
	
	@Test
	public void itShouldCalculateTheDamageForWihoutTheCriticalBonus() {
		/*
		 * DAMAGE = [(2 x 100 + 10) / 250 x (110 / 95) x (40)] + (2 * 1.0 * 1.0)
		 * DAMAGE = 99.2631
		 */
		setupRandomGeneratorForNotACriticalAttack(attackerStoneMissiles);
		double damage = logic.calculatePhysicalDamage(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		assertEquals(99.2631, damage, MAX_ERROR);
	}
	
	
	@Test
	public void itShouldDecreaseThePartDurabilityAfterAnAttack() {
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		double damage = logic.calculateTotalPhysicalDamage(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		logic.performPhysicalAttack(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		
		assertTrue(defenderLeftArm.getDurability() < defenderLeftArm.getOriginalDurability());
		
		double expectedDurabilityLoss = damage / 10.0;
		int expectedDurabilityAfterAttack = (int)(defenderLeftArm.getOriginalDurability() - expectedDurabilityLoss);
		
		assertEquals(expectedDurabilityAfterAttack, defenderLeftArm.getDurability());
	}
	
	@Test
	public void itShouldBreakAfterDurabilityReachedZero() {
		// First attack
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		logic.performPhysicalAttack(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		
		// Second attack
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		logic.performPhysicalAttack(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		
		assertEquals(0, defenderLeftArm.getDurability());
		assertTrue(defenderLeftArm.isBroken());
	}
	
	@Test
	public void itShouldDecreaseRobotResistanceAfterAnAttack() {
		double originalResistance = defender.getAttributes().getResistance();
		
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		logic.performPhysicalAttack(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		
		assertTrue(defender.getAttributes().getResistance() < originalResistance);
	}
	
	@Test
	public void itShouldCalculateCorrectlyRobotResistanceAfterAnAttack() {
		double originalResistance = defender.getAttributes().getResistance();
		
		double damage = logic.calculateTotalPhysicalDamage(attacker, defender, attackerChasingHand, defenderLeftArm);
		logic.performPhysicalAttack(attacker, defender, attackerChasingHand, defenderLeftArm);
		
		double expectedResistance = originalResistance - damage;
		assertEquals(expectedResistance, defender.getAttributes().getResistance(), MAX_ERROR);
	}
	
	@Test
	public void theResistanceShouldNeverBeUnderZero() {
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		logic.performPhysicalAttack(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		assertTrue(defender.getAttributes().getResistance() >= 0);
	}
	
	@Test
	public void oneRobotMustBeBrokenAfterItsResistanceReachesZero() {
		assertFalse(defender.isBroken());
		setupRandomGeneratorForACriticalAttack(attackerStoneMissiles);
		logic.performPhysicalAttack(attacker, defender, attackerStoneMissiles, defenderLeftArm);
		assertTrue(defender.isBroken());
	}
	
	private void setupRandomGeneratorForAMiss(Skill skill) {
		int aboveSkillValue = (int)(skill.getAccuracy() + 1);
		randomGenerator.setValue(aboveSkillValue);
	}
	
	private void setupRandomGeneratorForACriticalAttack(Skill skill) {
		int belowSkillValue = skill.getCriticalProbability() - 1;
		randomGenerator.setValue(belowSkillValue);
	}
	
	private void setupRandomGeneratorForNotACriticalAttack(Skill skill) {
		int belowSkillValue = skill.getCriticalProbability() + 1;
		randomGenerator.setValue(belowSkillValue);
	}
}
