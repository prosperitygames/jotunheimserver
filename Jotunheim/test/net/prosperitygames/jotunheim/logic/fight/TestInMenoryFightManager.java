package net.prosperitygames.jotunheim.logic.fight;

import static org.junit.Assert.*;
import static net.prosperitygames.jotunheim.test.JotunheimAssertions.*;

import net.prosperitygames.entities.Fight;
import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.logic.login.Authentication;
import net.prosperitygames.jotunheim.logic.login.InMemoryAuthentication;
import net.prosperitygames.jotunheim.logic.mail.MailServiceMock;
import net.prosperitygames.jotunheim.test.UserFactory;

import org.junit.Before;
import org.junit.Test;

public class TestInMenoryFightManager {
	private FightManager manager;
	private Authentication auth;
	
	@Before
	public void setUp() {
		manager = new InMemoryFightManager();
		auth    = new InMemoryAuthentication();
		
		((InMemoryAuthentication)auth).setMailService(new MailServiceMock());
		((InMemoryFightManager)manager).setAuthentication(auth);
		
		registerUser(UserFactory.correct());
		registerUser(UserFactory.anotherCorrect());
	}

	@Test
	public void itShouldCreateAFight() {
		User user = UserFactory.correct();
		Fight fight = manager.createFight(user.getLogin(), "Dummy Fight");
		assertNotNull(fight);
	}
	
	@Test(expected=UserNotFoundException.class)
	public void itShouldNotCreateAFightForANonExistingUsername() {
		manager.createFight("invalid", "Dummy Fight");
	}
	
	@Test(expected=UserHasAlreadyAnActiveFightException.class)
	public void itShouldNotAllowAUserToCreateTwoActiveFights() {
		User user = UserFactory.correct();
		manager.createFight(user.getLogin(), "Dummy Fight");
		manager.createFight(user.getLogin(), "Another Dummy Fight");
	}
	
	@Test
	public void aNewFightShouldAppearInAvailableFightsList() {
		User user = UserFactory.correct();
		
		assertNotNull(manager.listAvailableFights());
		assertIsEmpty(manager.listAvailableFights());
		
		Fight fight = manager.createFight(user.getLogin(), "Dummy Fight");
		
		assertIsNotEmpty(manager.listAvailableFights());
		assertTrue(manager.listAvailableFights().contains(fight));
	}
	
	@Test
	public void isShouldBePossibleToJoinAFight() {
		User user      = UserFactory.correct();
		User otherUser = UserFactory.anotherCorrect();
		
		Fight fight = manager.createFight(user.getLogin(), "Dummy Fight");
		manager.joinFight(fight.getId(), otherUser.getLogin());
	}
	
	@Test
	public void aFightWithTwoAssignedUsersShouldNotAppearInAvailableFightsList() {
		User user      = UserFactory.correct();
		User otherUser = UserFactory.anotherCorrect();
		
		Fight fight = manager.createFight(user.getLogin(), "Dummy Fight");
		manager.joinFight(fight.getId(), otherUser.getLogin());
		
		assertNotNull(manager.listAvailableFights());
		assertIsEmpty(manager.listAvailableFights());
	}
	
	@Test(expected=UserNotFoundException.class)
	public void isShouldNotAllowAnUnexistingUserToJoinAFight() {
		User user = UserFactory.correct();
		
		Fight fight = manager.createFight(user.getLogin(), "Dummy Fight");
		manager.joinFight(fight.getId(), "nobody");
	}
	
	@Test(expected=FightNotAvailableException.class)
	public void itShouldNotAllowAUserToJoinANotAvailableFight() {
		User user      = UserFactory.correct();
		User otherUser = UserFactory.anotherCorrect();
		
		Fight fight = manager.createFight(user.getLogin(), "Dummy Fight");
		manager.joinFight(fight.getId(), otherUser.getLogin());
		
		User anotherUser = UserFactory.oneMoreCorrect();
		registerUser(anotherUser);
		manager.joinFight(fight.getId(), anotherUser.getLogin());
	}

	private void registerUser(User user) {
		auth.register(user.getName(), user.getEmail(), user.getLogin(), user.getPassword());
	}
}
