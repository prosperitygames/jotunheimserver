package net.prosperitygames.jotunheim.logic.login;

import static org.junit.Assert.*;
import static net.prosperitygames.jotunheim.test.JotunheimAssertions.*;
import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.logic.context.GameContext;
import net.prosperitygames.jotunheim.logic.mail.MailServiceMock;
import net.prosperitygames.jotunheim.test.UserFactory;

import org.codemonkey.simplejavamail.Email;
import org.junit.Before;
import org.junit.Test;

public class TestInMemoryAuthentication {
	private static final String CLIENT_ID_A = "0";
	private static final String CLIENT_ID_B = "1";
	
	private Authentication auth;
	private MailServiceMock mailService;
	
	@Before
	public void setUp() {
		auth        = new InMemoryAuthentication();
		mailService = new MailServiceMock();
		((InMemoryAuthentication)auth).setMailService(mailService);
	}
	
	@Test
	public void testRegisterAndLoginSuccessfully() {
		assertFalse("Should not exist an user at game context", GameContext.getInstance(CLIENT_ID_A).hasKey("user"));
		registerAndLogin(GameContext.getInstance(CLIENT_ID_A), UserFactory.correct());
		assertTrue("After login, should exist an user at game context", GameContext.getInstance(CLIENT_ID_A).hasKey("user"));
	}

	@Test(expected=EmptyFieldException.class)
	public void testRegisterWithBlankFields() {
		auth.register("", "", "", "");
	}
	
	@Test(expected=InvalidEmailException.class)
	public void testRegisterWithInvalidEmail() {
		User user = UserFactory.invalidEmail();
		register(user);
	}
	
	@Test(expected=RepeatedLoginException.class)
	public void testRegisterTwoUsersWithSameLogin() {
		User userA = UserFactory.correct();
		User userB = UserFactory.correct();
		
		register(userA);
		register(userB);
	}
	
	@Test(expected=EmailAlreadyTakenException.class)
	public void testRegisterTwoUsersWithSameEmail() {
		User userA = UserFactory.correct();
		User userB = UserFactory.anotherCorrect();
		
		userB.setEmail(userA.getEmail());
		
		register(userA);
		register(userB);
	}
	
	@Test(expected=AuthenticationException.class)
	public void testLoginWithNoRegisteredUsers() {
		auth.login("johndoe", "johndoe", GameContext.getInstance(CLIENT_ID_A));
	}
	
	@Test(expected=AuthenticationException.class)
	public void testRegisterAndLoginWrongLogin() {
		auth.register("John Doe", "john_doe@example.com", "johndoe", "johndoe");
		auth.login("janepoe", "johndoe", GameContext.getInstance(CLIENT_ID_A));
	}
	
	@Test(expected=AuthenticationException.class)
	public void testRegisterAndLoginRightUsernameWrongPassword() {
		auth.register("John Doe", "john_doe@example.com", "johndoe", "johndoe");
		auth.login("johndoe", "janepoe", GameContext.getInstance(CLIENT_ID_A));
	}
	
	@Test
	public void itShouldAllowTwoDifferentLoginsForTwoDifferentSessions() {
		User userA = UserFactory.correct();
		User userB = UserFactory.anotherCorrect();
		
		registerAndLogin(GameContext.getInstance(CLIENT_ID_A), userA);
		registerAndLogin(GameContext.getInstance(CLIENT_ID_B), userB);
		
		assertTrue("After login, should exist an user at game context", GameContext.getInstance(CLIENT_ID_A).hasKey("user"));
		assertTrue("After login, should exist an user at game context", GameContext.getInstance(CLIENT_ID_B).hasKey("user"));
		
		GameContext ctxA = GameContext.getInstance(CLIENT_ID_A);
		GameContext ctxB = GameContext.getInstance(CLIENT_ID_B);
		
		assertEquals("The context A should hold the info of user A", ((User)ctxA.getAttribute("user")).getLogin(), userA.getLogin());
		assertEquals("The context B should hold the info of user B", ((User)ctxB.getAttribute("user")).getLogin(), userB.getLogin());
	}
	
	@Test
	public void itShouldNotFindANotRegisteredUser() {
		assertFalse(auth.userExists(UserFactory.correct().getLogin()));
	}
	
	@Test(expected=AuthenticationException.class)
	public void itShouldThrowAnExceptionIfOneTriesToRetrieveAnUnexistingUser() {
		auth.getUser(UserFactory.correct().getLogin());
	}
	
	@Test
	public void itShouldSendAnEmailToANewlyRegisteredUser() {
		User user = UserFactory.correct();
		
		assertIsEmpty(mailService.getSentMails());
		registerAndLogin(GameContext.getInstance(CLIENT_ID_A), user);
		assertIsNotEmpty(mailService.getSentMails());
		assertEquals(1, mailService.getSentMails().size());
		
		Email email = mailService.getSentMails().get(0);
		assertEquals(user.getEmail(), email.getRecipients().get(0).getAddress());
	}

	private void register(User user) {
		auth.register(user.getName(), user.getEmail(), user.getLogin(), user.getPassword());
	}
	
	private void registerAndLogin(GameContext context, User user) {
		register(user);
		auth.login(user.getLogin(), user.getPassword(), context);
	}
}
