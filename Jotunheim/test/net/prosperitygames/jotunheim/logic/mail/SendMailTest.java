package net.prosperitygames.jotunheim.logic.mail;

import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage.RecipientType;

import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.util.JotunheimToolkit;

import org.codemonkey.simplejavamail.Email;
import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;

public class SendMailTest {
	public static void main(String[] args) throws Exception {
//		Scanner keyboard = new Scanner(System.in);
//		
//		System.out.print("Type your email (gmail only): ");
//		String yourEmail = keyboard.nextLine();
//		
//		System.out.print("Type your password: ");
//		String yourPassword = keyboard.nextLine();
		
		Email email = new Email();

		User user = new User();
		user.setEmail("brunogamacatao@gmail.com");
		user.setName("Jemaum");
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("user", user);
		
		email.setFromAddress("Prosperity Games", "no-reply@prosperitygames.net");
		email.setSubject("Bem-Vindo a Project Jotunheim");
		email.addRecipient(user.getName(), user.getEmail(), RecipientType.TO);
		email.setText(JotunheimToolkit.render("new_user_email.html", model));
		email.setTextHTML(JotunheimToolkit.render("new_user_email.html", model));
		
		email.addEmbeddedImage("prosperity_logo", JotunheimToolkit.getAttachment("prosperity_white.png"), "image/png");
		email.addEmbeddedImage("jotunheim_logo", JotunheimToolkit.getAttachment("JP_logo.png"), "image/png");
		
		new Mailer("smtp.prosperitygames.net", 587, "no-reply@prosperitygames.net", "jotun8852", TransportStrategy.SMTP_PLAIN).sendMail(email);
	}
}
