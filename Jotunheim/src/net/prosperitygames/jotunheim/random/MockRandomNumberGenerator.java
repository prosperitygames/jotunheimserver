package net.prosperitygames.jotunheim.random;

import lombok.Setter;

public class MockRandomNumberGenerator implements RandomNumberGenerator {
	@Setter
	private int value;
	
	@Override
	public int randInt(int min, int max) {
		return value;
	}
}
