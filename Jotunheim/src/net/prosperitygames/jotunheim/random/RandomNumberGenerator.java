package net.prosperitygames.jotunheim.random;

public interface RandomNumberGenerator {
	public int randInt(int min, int max);
}
