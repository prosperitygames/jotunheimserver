package net.prosperitygames.jotunheim.logic.login;

public class InvalidEmailException extends RuntimeException {
	private static final long serialVersionUID = -3279235862879531644L;

	public InvalidEmailException(String email) {
		super(String.format("The specified email (%s) is invalid", email));
	}
}
