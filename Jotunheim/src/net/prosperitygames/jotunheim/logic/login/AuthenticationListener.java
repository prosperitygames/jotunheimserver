package net.prosperitygames.jotunheim.logic.login;

public interface AuthenticationListener {
	public void loginPerformed(String clientId);
	public void logoutPerformed(String clientId);
}
