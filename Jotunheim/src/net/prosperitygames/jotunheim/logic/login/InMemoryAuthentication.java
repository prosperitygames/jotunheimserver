package net.prosperitygames.jotunheim.logic.login;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage.RecipientType;

import lombok.Getter;
import lombok.Setter;
import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.logic.context.GameContext;
import net.prosperitygames.jotunheim.logic.mail.MailService;
import net.prosperitygames.jotunheim.util.JotunheimToolkit;

import org.apache.commons.lang.StringUtils;
import org.codemonkey.simplejavamail.Email;

public class InMemoryAuthentication implements Authentication {
	@Getter @Setter
	private MailService mailService;
	
	private Map<String, User> users;
	private List<AuthenticationListener> listeners;
	
	public InMemoryAuthentication() {
		users     = new HashMap<String, User>();
		listeners = new ArrayList<AuthenticationListener>();
		registerDefaultAdminAndTestUsers();
	}

	@Override
	public void login(String username, String password, GameContext gameContext) {
		if (!users.containsKey(username)) {
			throw new AuthenticationException(AuthenticationException.NO_SUCH_USERNAME);
		}
		
		if (!users.get(username).getPassword().equals(password)) {
			throw new AuthenticationException(AuthenticationException.WRONG_PASSWORD);
		}
		
		gameContext.setAttribute("user", users.get(username));
		
		for (AuthenticationListener listener : listeners) {
			listener.loginPerformed(gameContext.getClientId());
		}
	}
	
	@Override
	public void logout(String clientId) {
		for (AuthenticationListener listener : listeners) {
			listener.logoutPerformed(clientId);
		}
		GameContext.removeContext(clientId);
	}

	@Override
	public void register(String name, String email, String username, String password) {
		this.register(name, email, username, password, true);
	}
	
	public void register(String name, String email, String username, String password, boolean sendConfirmationEmail) {
		if (StringUtils.isEmpty(name))                throw new EmptyFieldException("name");
		if (StringUtils.isEmpty(email))               throw new EmptyFieldException("email");
		if (StringUtils.isEmpty(username))            throw new EmptyFieldException("username");
		if (StringUtils.isEmpty(password))            throw new EmptyFieldException("password");
		if (!JotunheimToolkit.isACorrectEmail(email)) throw new InvalidEmailException(email);
		if (userExists(username))                     throw new RepeatedLoginException();
		if (isThisEmailAlreadyTaken(email))           throw new EmailAlreadyTakenException();
		
		User user = new User();
		
		user.setId((long)users.size());
		user.setName(name);
		user.setEmail(email);
		user.setLogin(username);
		user.setPassword(password);
		
		users.put(username, user);
		
		if (sendConfirmationEmail) {
			user.setConfirmedEmail(true);
			sendInvitationEmail(user);
		} else {
			user.setConfirmedEmail(false);
		}
	}
	
	private boolean isThisEmailAlreadyTaken(String email) {
		for (String username : users.keySet()) {
			User user = users.get(username);
			if (user.getEmail().equals(email)) {
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean userExists(String username) {
		return users.containsKey(username);
	}

	@Override
	public User getUser(String username) {
		if (!userExists(username)) {
			throw new AuthenticationException(AuthenticationException.NO_SUCH_USERNAME);
		}
		return users.get(username);
	}
	
	private void registerDefaultAdminAndTestUsers() {
		register("admin", "admin@jotunheim.com", "admin", "admin", false);
		register("test", "test@jotunheim.com", "test", "test", false);
	}

	@Override
	public void addAuthenticationListener(AuthenticationListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeAuthenticationListener(AuthenticationListener listener) {
		listeners.remove(listener);
	}
	
	private void sendInvitationEmail(User user) {
		Email email = new Email();
		email.addRecipient(user.getName(), user.getEmail(), RecipientType.TO);
		
		if (mailService != null) {
			mailService.sendMail(email);
		}
	}
}
