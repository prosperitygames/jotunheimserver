package net.prosperitygames.jotunheim.logic.login;

public class AuthenticationException extends RuntimeException {
	private static final long serialVersionUID = -5817855140135905286L;

	public static final String NO_SUCH_USERNAME = "The provied username could not be found";
	public static final String WRONG_PASSWORD   = "The provied username and/or password is wrong";
	
	public AuthenticationException(String message) {
		super(message);
	}
}
