package net.prosperitygames.jotunheim.logic.login;

public class EmailAlreadyTakenException extends RuntimeException{
	private static final long serialVersionUID = 409978209658720508L;
	
	public EmailAlreadyTakenException() {
		super("The specified email is already being used");
	}
}
