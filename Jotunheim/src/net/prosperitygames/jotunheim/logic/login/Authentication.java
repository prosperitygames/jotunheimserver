package net.prosperitygames.jotunheim.logic.login;

import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.logic.context.GameContext;

public interface Authentication {
	public void login(String username, String password, GameContext gameContext);
	public void logout(String clientId);
	public void register(String name, String email, String username, String password);
	public boolean userExists(String username);
	public User getUser(String username);
	public void addAuthenticationListener(AuthenticationListener listener);
	public void removeAuthenticationListener(AuthenticationListener listener);
}
