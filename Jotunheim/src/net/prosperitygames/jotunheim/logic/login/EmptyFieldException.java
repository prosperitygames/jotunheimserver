package net.prosperitygames.jotunheim.logic.login;

public class EmptyFieldException extends RuntimeException {
	private static final long serialVersionUID = -8007508155178585563L;

	public EmptyFieldException(String field) {
		super("The field " + field + " should not be empty");
	}
}
