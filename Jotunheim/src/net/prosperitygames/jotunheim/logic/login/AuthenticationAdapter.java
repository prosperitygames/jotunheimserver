package net.prosperitygames.jotunheim.logic.login;

public abstract class AuthenticationAdapter implements AuthenticationListener {
	@Override
	public void loginPerformed(String clientId) {
	}

	@Override
	public void logoutPerformed(String clientId) {
	}
}
