package net.prosperitygames.jotunheim.logic.login;

public class RepeatedLoginException extends RuntimeException {
	private static final long serialVersionUID = -3335161614905978696L;
	
	public RepeatedLoginException() {
		super("The chosen username is already taken");
	}
}
