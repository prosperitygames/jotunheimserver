package net.prosperitygames.jotunheim.logic.context;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

public class GameContext {
	private static Map<String, GameContext> instances = new HashMap<String, GameContext>();
	
	public static GameContext getInstance(String clientId) {
		if (!instances.containsKey(clientId)) {
			instances.put(clientId, new GameContext(clientId));
		}
		
		return instances.get(clientId);
	}
	
	public static void removeContext(String clientId) {
		instances.remove(clientId);
	}

	@Getter @Setter
	private String clientId;
	private Map<String, Object> attributes;
	
	private GameContext(String clientId) {
		this.clientId = clientId;
		attributes    = new HashMap<String, Object>();
	}
	
	public void setAttribute(String key, Object value) {
		attributes.put(key, value);
	}

	public Object getAttribute(String key) {
		return attributes.get(key);
	}

	public boolean hasKey(String key) {
		return attributes.containsKey(key);
	}
}
