package net.prosperitygames.jotunheim.logic.mail;

import lombok.Getter;
import lombok.Setter;

import org.codemonkey.simplejavamail.Email;
import org.codemonkey.simplejavamail.Mailer;
import org.codemonkey.simplejavamail.TransportStrategy;

public class MailServiceImpl implements MailService {
	@Getter @Setter
	private String senderName;
	@Getter @Setter
	private String username;
	@Getter @Setter
	private String password;
	@Getter @Setter
	private String smtpServer;
	@Getter @Setter
	private int port;
	
	@Override
	public void sendMail(Email email) {
		createMailer().sendMail(email);
	}
	
	private Mailer createMailer() {
		return new Mailer(smtpServer, port, username, password, TransportStrategy.SMTP_SSL);
	}
}
