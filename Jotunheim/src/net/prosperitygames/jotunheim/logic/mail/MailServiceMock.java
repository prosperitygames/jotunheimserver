package net.prosperitygames.jotunheim.logic.mail;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.codemonkey.simplejavamail.Email;

public class MailServiceMock implements MailService {
	@Getter @Setter
	private List<Email> sentMails;
	
	public MailServiceMock() {
		sentMails = new ArrayList<Email>();
	}
	
	@Override
	public void sendMail(Email email) {
		sentMails.add(email);
	}
}
