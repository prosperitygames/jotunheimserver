package net.prosperitygames.jotunheim.logic.mail;

import org.codemonkey.simplejavamail.Email;

public interface MailService {
	public void sendMail(Email email);
}
