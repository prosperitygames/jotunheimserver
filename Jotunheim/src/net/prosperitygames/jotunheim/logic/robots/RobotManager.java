package net.prosperitygames.jotunheim.logic.robots;

import java.util.List;

import net.prosperitygames.entities.Robot;

public interface RobotManager {
	void addRobot(Robot robot);
	List<Robot> getRobots();
	Robot findRobotByName(String robotName);
}
