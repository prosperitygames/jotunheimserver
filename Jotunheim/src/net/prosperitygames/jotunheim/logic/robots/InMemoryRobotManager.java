package net.prosperitygames.jotunheim.logic.robots;

import java.util.ArrayList;
import java.util.List;

import net.prosperitygames.entities.Robot;
import net.prosperitygames.jotunheim.util.RobotFactory;

public class InMemoryRobotManager implements RobotManager {
	private static long nextRobotId = 0;
	private List<Robot> robots;
	
	public InMemoryRobotManager() {
		robots = new ArrayList<Robot>();
		createTestRobots();
	}

	public void addRobot(Robot robot) {
		robot.setId(nextRobotId++);
		robots.add(robot);
	}
	
	public List<Robot> getRobots() {
		return robots;
	}
	
	public Robot findRobotByName(String robotName) {
		for (Robot robot : robots) {
			if (robot.getName().equalsIgnoreCase(robotName)) {
				return robot;
			}
		}
		return null;
	}
	
	private void createTestRobots() {
		try {
			addRobot(RobotFactory.loadRobot("beowulf.xml"));
			addRobot(RobotFactory.loadRobot("dagr.xml"));
			addRobot(RobotFactory.loadRobot("fenrir.xml"));
		} catch (Exception ex) {
			ex.printStackTrace();
			System.err.println(ex);
			throw new RuntimeException("Could not load the robots from file");
		}
	}
}
