package net.prosperitygames.jotunheim.logic.events;

import net.prosperitygames.entities.Fight;

public interface FightListener {
	public void fightStarted(Fight fight);
}
