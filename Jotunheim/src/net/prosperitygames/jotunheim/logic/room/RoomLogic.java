package net.prosperitygames.jotunheim.logic.room;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import net.prosperitygames.entities.Room;
import net.prosperitygames.jotunheim.logic.login.Authentication;

public class RoomLogic {
	@Getter @Setter
	private Authentication authentication;
	private List<Room> rooms;
	private List<GlobalRoomListener> globalListeners;
	private Map<Long, List<Room>> listeners;
	
	public RoomLogic() {
		rooms           = new ArrayList<Room>();
		globalListeners = new ArrayList<GlobalRoomListener>();
		listeners       = new HashMap<Long, List<Room>>();
	}
	
	public Room create(String ownerLogin, String roomName, int capacity) {
		Room room = new Room();
		
		room.setOwner(authentication.getUser(ownerLogin));
		room.setName(roomName);
		room.setCapacity(capacity);
		
		rooms.add(room);
		
		return room;
	}
	
	public List<Room> list() {
		return rooms;
	}
	
	public void cancel() {
		
	}
	
	public void join() {
		
	}
	
	public void exit() {
		
	}
	
	public void start() {
		
	}
}
