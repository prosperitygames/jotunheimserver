package net.prosperitygames.jotunheim.logic.room;

import net.prosperitygames.entities.Room;

public interface GlobalRoomListener {
	void roomCreated(Room room);
	void roomStarted(Room room);
	void roomCancelled(Room room);
}
