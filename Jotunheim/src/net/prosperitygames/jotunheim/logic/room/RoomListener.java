package net.prosperitygames.jotunheim.logic.room;

import net.prosperitygames.entities.User;

public interface RoomListener {
	void roomStarted();
	void roomCancelled();
	void userJoined(User user);
	void userLeft(User user);
}
