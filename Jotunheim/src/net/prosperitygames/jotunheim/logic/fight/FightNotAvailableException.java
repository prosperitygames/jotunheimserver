package net.prosperitygames.jotunheim.logic.fight;

public class FightNotAvailableException extends RuntimeException {
	private static final long serialVersionUID = 6203565510798055664L;

	public FightNotAvailableException() {
		super("This fight is not available");
	}
}
