package net.prosperitygames.jotunheim.logic.fight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import lombok.Getter;
import net.prosperitygames.entities.AttackType;
import net.prosperitygames.entities.Fight;
import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.logic.context.GameContext;
import net.prosperitygames.jotunheim.logic.login.Authentication;
import net.prosperitygames.jotunheim.logic.login.AuthenticationAdapter;

public class InMemoryFightManager implements FightManager {
	private static long nextFightId = 0;
	
	@Getter
	private Authentication authentication;
	private List<Fight> fights;
	private List<FightManagerListener> listeners;
	
	public InMemoryFightManager() {
		fights    = new ArrayList<Fight>();
		listeners = new ArrayList<FightManagerListener>();
	}
	
	public void createDemoFights() {
		List<String> robots = Arrays.asList(new String[]{"Beowulf", "Njord", "Black Beowulf"});
		User testUser       = authentication.getUser("test");
		Fight fight         = this.createFight(testUser.getLogin(), "Test Fight");
		this.setRobots(testUser, fight.getId(), robots);
	}

	@Override
	public Fight createFight(String username, String fightname) {
		if (!authentication.userExists(username)) throw new UserNotFoundException();
		if (thisUserHasAnActiveFight(username)) throw new UserHasAlreadyAnActiveFightException();
		
		Fight fight = new Fight();
		
		fight.setId(nextFightId++);
		fight.setName(fightname);
		fight.setOwner(authentication.getUser(username));
		
		fights.add(fight);
		
		for (FightManagerListener listener : listeners) {
			listener.fightCreated(fight);
		}
		
		return fight;
	}

	@Override
	public List<Fight> listAvailableFights() {
		List<Fight> availableFights = new ArrayList<Fight>();
		
		for (Fight fight : fights) {
			if (fight.getOpponent() == null) {
				availableFights.add(fight);
			}
		}
		
		return availableFights;
	}

	@Override
	public void joinFight(long fightId, String username) {
		if (!authentication.userExists(username)) throw new UserNotFoundException();
		
		Fight fight = getFight(fightId);
		if (fight.getOpponent() != null) {
			throw new FightNotAvailableException();
		}
		
		fight.setOpponent(authentication.getUser(username));		
	}

	private boolean thisUserHasAnActiveFight(String username) {
		for (Fight fight : fights) {
			if (fight.getOwner().getLogin().equals(username) ||
				(fight.getOpponent() != null && 
				fight.getOpponent().getLogin().equals(username))) {
				return true;
			}
		}
		return false;
	}
	
	private Fight getFight(long fightId) {
		for (Fight fight : fights) {
			if (fight.getId() == fightId) {
				return fight;
			}
		}
		
		return null;
	}

	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
		authentication.addAuthenticationListener(new AuthenticationAdapter() {
			public void logoutPerformed(String clientId) {
				removeFightsFromUser((User)GameContext.getInstance(clientId).getAttribute("user"));
			}
		});
	}
	
	private void removeFightsFromUser(User user) {
		for (Iterator<Fight> it = fights.iterator(); it.hasNext(); ) {
			Fight fight = it.next();
			if (fight.getOwner().getId() == user.getId() || 
				(fight.getOpponent() != null && fight.getOpponent().getId() == user.getId())) {
				it.remove();
				
				for (FightManagerListener listener : listeners) {
					listener.fightRemoved(fight);
				}
			}
			
		}
	}

	@Override
	public void addFightManagerListener(FightManagerListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeFightManagerListener(FightManagerListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void registerForFight(User user, long fightId) {
		Fight f = findFightById(fightId);
		f.setOpponent(user);
	}

	@Override
	public void registerAttack(long fightId, AttackType type, String attackerId, String defenderId, String attackerPartId, String defenderPartId) {
		Fight f = findFightById(fightId);
		f.registerAttack(type, attackerId, defenderId, attackerPartId, defenderPartId);
	}
	
	public void registerBuff(String clientId, long fightId, String attackerId, String attackerPartId) {
		Fight f = findFightById(fightId);
		f.registerBuff(clientId, fightId, attackerId, attackerPartId);
	}

	@Override
	public Fight findFightById(long fightId) {
		int pos = fights.indexOf(new Fight(fightId));
		return pos >= 0 ? this.fights.get(pos) : null;
	}

	@Override
	public void setRobots(User user, long fightId, List<String> robots) {
		Fight f = findFightById(fightId);
		f.setRobots(user, robots);
		if (f.isComplete()) {
			for (FightManagerListener listener : listeners) {
				listener.fightStartedRemoveItFromAvailableList(f);
			}
		}
	}
}
