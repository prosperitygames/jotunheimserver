package net.prosperitygames.jotunheim.logic.fight;

public class UserHasAlreadyAnActiveFightException extends RuntimeException {
	private static final long serialVersionUID = 4501272716979088034L;

	public UserHasAlreadyAnActiveFightException() {
		super("There's already an active fight assigned to this user");
	}
}
