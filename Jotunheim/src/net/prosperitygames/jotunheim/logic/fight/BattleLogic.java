package net.prosperitygames.jotunheim.logic.fight;

import lombok.Getter;
import lombok.Setter;
import net.prosperitygames.entities.Robot;
import net.prosperitygames.entities.RobotPart;
import net.prosperitygames.entities.Skill;
import net.prosperitygames.jotunheim.random.RandomNumberGenerator;

/**
 * The purpose of this class is to encapsulate the whole battle logic of 
 * Jotunheim Project.
 */
public class BattleLogic {
	private static final double MISS = 0.0;
	
	@Getter @Setter
	private RandomNumberGenerator randomNumberGenerator;
	
	public double calculatePhysicalDamage(Robot attacker, Robot defender, Skill sourceSkill, RobotPart target) {
		if (sourceSkill.getAccuracy() < randomNumberGenerator.randInt(0, 100)) {
			return MISS;
		}
		
		double perLevelDamage     = (2.0 * attacker.getLevel() + 10.0) / 250.0;
		double attackDefenseRatio = attacker.getAttributes().getPhysicalAttack() / defender.getAttributes().getPhysicalDefense();
		double skillTypeInfluence = sourceSkill.getType().getAttackStrengthFactor(target.getSkill().getType());
		double criticalBonus      = isItACriticalAttack(sourceSkill) ? 2.0 : 1.0;
		
		
		return (perLevelDamage * attackDefenseRatio * sourceSkill.getBasePower()) + (2 * skillTypeInfluence * criticalBonus);
	}

	public double calculateTotalPhysicalDamage(Robot attacker, Robot defender, Skill sourceSkill, RobotPart target) {
		double damage = calculatePhysicalDamage(attacker, defender, sourceSkill, target);
		
		double totalPercentage = 0.0;
		
		for (RobotPart part : attacker.getParts()) {
			totalPercentage += part.getSkill().getImpactOnEnemy().getPhysicalAttack();
		}
		
		return damage + (damage * totalPercentage) / 100.0;
	}

	public void performPhysicalAttack(Robot attacker, Robot defender, Skill sourceSkill, RobotPart target) {
		double totalDamage = calculateTotalPhysicalDamage(attacker, defender, sourceSkill, target);
		
		updatePartDurability(totalDamage, target);
		updateTargetResistance(totalDamage, defender);
	}

	private void updateTargetResistance(double totalDamage, Robot defender) {
		double resistance = defender.getAttributes().getResistance();
		resistance -= totalDamage;
		
		if (resistance < 0) {
			resistance = 0;
			defender.setBroken(true);
		}
		
		defender.getAttributes().setResistance(resistance);
	}

	private void updatePartDurability(double totalDamage, RobotPart target) {
		double durabilityLoss = totalDamage / 10.0;
		int finalDurability   = (int)(target.getDurability() - durabilityLoss);
		
		if (finalDurability <= 0) {
			finalDurability = 0;
			target.setBroken(true);
		}
		
		target.setDurability(finalDurability);
	}
	
	private boolean isItACriticalAttack(Skill sourceSkill) {
		if (sourceSkill.getCriticalProbability() <= 0) {
			return false;
		}
		
		int diceValue = randomNumberGenerator.randInt(0, 100);
		return diceValue <= sourceSkill.getCriticalProbability();
	}
}
