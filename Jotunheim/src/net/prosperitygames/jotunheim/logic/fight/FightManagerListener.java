package net.prosperitygames.jotunheim.logic.fight;

import net.prosperitygames.entities.Fight;

public interface FightManagerListener {
	public void fightCreated(Fight fight);
	public void fightRemoved(Fight fight);
	public void fightStartedRemoveItFromAvailableList(Fight fight);
}
