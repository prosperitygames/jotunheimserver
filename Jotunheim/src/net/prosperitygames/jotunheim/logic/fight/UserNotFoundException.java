package net.prosperitygames.jotunheim.logic.fight;

public class UserNotFoundException extends RuntimeException {
	private static final long serialVersionUID = -26251767759792042L;

	public UserNotFoundException() {
		super("The specified username could not be found");
	}
}
