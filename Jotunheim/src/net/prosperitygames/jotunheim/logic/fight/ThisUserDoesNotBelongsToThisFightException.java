package net.prosperitygames.jotunheim.logic.fight;

public class ThisUserDoesNotBelongsToThisFightException extends RuntimeException {
	private static final long serialVersionUID = 2508998689024015225L;

	public ThisUserDoesNotBelongsToThisFightException() {
		super("The provided user does not belongs to the selected fight");
	}
}
