package net.prosperitygames.jotunheim.logic.fight;

import java.util.List;

import net.prosperitygames.entities.AttackType;
import net.prosperitygames.entities.Fight;
import net.prosperitygames.entities.User;

public interface FightManager {
	public Fight createFight(String username, String fightname);
	public void joinFight(long fightId, String username);
	public List<Fight> listAvailableFights();
	public void addFightManagerListener(FightManagerListener listener);
	public void removeFightManagerListener(FightManagerListener listener);
	public void registerAttack(long fightId, AttackType type, String attackerId, String defenderId, String attackerPartId, String defenderPartId);
	public Fight findFightById(long fightId);
	public void setRobots(User user, long fightId, List<String> robots);
	public void registerForFight(User user, long fightId);
}
