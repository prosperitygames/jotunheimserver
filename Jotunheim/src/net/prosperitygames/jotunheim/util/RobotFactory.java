package net.prosperitygames.jotunheim.util;

import java.io.File;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import net.prosperitygames.entities.AttackType;
import net.prosperitygames.entities.Robot;
import net.prosperitygames.entities.RobotPart;
import net.prosperitygames.entities.RobotPartQuality;
import net.prosperitygames.entities.RobotPartType;
import net.prosperitygames.entities.Skill;
import net.prosperitygames.entities.SkillRange;
import net.prosperitygames.entities.SkillType;

public class RobotFactory {
	private static final String BASE_PACKAGE = "/net/prosperitygames/jotunheim/data/";
	
	public static Robot loadRobot(String filename) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(Robot.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		InputStream inputStream = RobotFactory.class.getResourceAsStream(BASE_PACKAGE + filename);
		return (Robot)jaxbUnmarshaller.unmarshal(inputStream);
	}
	
	private static Robot createBeowulf() {
		Robot beowulf = new Robot();
		
		beowulf.setId(0L);
		beowulf.setLevel(100);
		beowulf.setName("Beowulf");
		beowulf.setDescription("The almighty Beowulf");
		beowulf.setRobotClass(SkillType.WARRIOR);
		beowulf.setMainSkillType(SkillType.BALMIUM);
		beowulf.setImageIconURL("../assets/icons/Beowulf.png");
		beowulf.setImageURL("../assets/images/Beowulf.png");
		beowulf.getAttributes().setResistance(105);
		beowulf.getAttributes().setPhysicalAttack(110);
		beowulf.getAttributes().setPhysicalDefense(95);
		beowulf.getAttributes().setRunicAttack(70);
		beowulf.getAttributes().setRunicDefense(100);
		beowulf.getAttributes().setMobility(75);
		
		RobotPart head = new RobotPart();
		head.setId(0L);
		head.setType(RobotPartType.HEAD);
		head.setImageURL("../assets/images/Beowulf_Head.png");
		head.setRefinement(0);
		head.setQuality(RobotPartQuality.COMMON);
		
		Skill powerBlessing = new Skill();
		powerBlessing.setId(0L);
		powerBlessing.setName("Power Blessing");
		powerBlessing.setRange(SkillRange.ONE_ENEMY);
		powerBlessing.setAccuracy(100);
		powerBlessing.getImpactOnMyself().setResistance(-10);
		powerBlessing.getImpactOnEnemy().setPhysicalAttack(30);
		powerBlessing.getImpactOnEnemy().setRunicAttack(30);
		
		head.setSkill(powerBlessing);
		beowulf.getParts().add(head);
		
		RobotPart legs = new RobotPart();
		legs.setId(1L);
		legs.setType(RobotPartType.LEGS);
		legs.setImageURL("../assets/images/Beowulf_Legs.png");
		legs.setRefinement(0);
		legs.setQuality(RobotPartQuality.COMMON);
		
		Skill earthquake = new Skill();
		earthquake.setId(1L);
		earthquake.setName("Earthquake");
		earthquake.setType(SkillType.EARTH);
		earthquake.setRange(SkillRange.ALL_PLAYERS);
		earthquake.setAttackType(AttackType.PHYSICAL);
		earthquake.setAccuracy(100);
		earthquake.setBasePower(100);
		
		legs.setSkill(earthquake);
		beowulf.getParts().add(legs);
		
		RobotPart rightArm = new RobotPart();
		rightArm.setId(2L);
		rightArm.setType(RobotPartType.RIGHT_ARM);
		rightArm.setImageURL("../assets/images/Beowulf_Right_Arm.png");
		rightArm.setRefinement(0);
		rightArm.setQuality(RobotPartQuality.COMMON);
		
		Skill counterAttack = new Skill();
		counterAttack.setId(2L);
		counterAttack.setName("Counter Attack");
		counterAttack.setType(SkillType.LIGHT);
		counterAttack.setRange(SkillRange.ONE_ENEMY);
		counterAttack.setAttackType(AttackType.PHYSICAL);
		counterAttack.setAccuracy(100);
		counterAttack.setBasePower(80);
		counterAttack.setCounterAttack(true);
		
		rightArm.setSkill(counterAttack);
		beowulf.getParts().add(rightArm);
		
		RobotPart leftArm = new RobotPart();
		leftArm.setId(3L);
		leftArm.setType(RobotPartType.LEFT_ARM);
		leftArm.setImageURL("../assets/images/Beowulf_Left_Arm.png");
		leftArm.setRefinement(0);
		leftArm.setQuality(RobotPartQuality.COMMON);
		
		Skill chasingHand = new Skill();
		chasingHand.setId(3L);
		chasingHand.setName("Chasing Hand");
		chasingHand.setType(SkillType.LIGHT);
		chasingHand.setRange(SkillRange.ONE_ENEMY);
		chasingHand.setAttackType(AttackType.PHYSICAL);
		chasingHand.setAccuracy(100);
		chasingHand.setBasePower(40);
		
		leftArm.setSkill(chasingHand);
		beowulf.getParts().add(leftArm);
		
		RobotPart chest = new RobotPart();
		chest.setId(4L);
		chest.setType(RobotPartType.CHEST);
		chest.setImageURL("../assets/images/Beowulf_Chest.png");
		chest.setRefinement(0);
		chest.setQuality(RobotPartQuality.COMMON);
		
		Skill stoneMissiles = new Skill();
		stoneMissiles.setId(4L);
		stoneMissiles.setName("Stone Missiles");
		stoneMissiles.setType(SkillType.STONE);
		stoneMissiles.setRange(SkillRange.ONE_ENEMY);
		stoneMissiles.setAttackType(AttackType.PHYSICAL);
		stoneMissiles.setAccuracy(80);
		stoneMissiles.setBasePower(100);
		stoneMissiles.setCriticalProbability(10);
		
		chest.setSkill(stoneMissiles);
		beowulf.getParts().add(chest);
		
		return beowulf;
	}
	
	private static Robot createDagr() {
		Robot dagr = new Robot();
		
		dagr.setId(1L);
		dagr.setLevel(100);
		dagr.setName("Dagr");
		dagr.setDescription("Dagr");
		dagr.setRobotClass(SkillType.WARRIOR);
		dagr.setMainSkillType(SkillType.LIGHT);
		dagr.setImageIconURL("../assets/icons/Beowulf.png");
		dagr.setImageURL("../assets/images/Dagr.png");
		dagr.getAttributes().setResistance(100);
		dagr.getAttributes().setPhysicalAttack(60);
		dagr.getAttributes().setPhysicalDefense(100);
		dagr.getAttributes().setRunicAttack(60);
		dagr.getAttributes().setRunicDefense(130);
		dagr.getAttributes().setMobility(70);
		
		RobotPart head = new RobotPart();
		head.setId(5L);
		head.setType(RobotPartType.HEAD);
		head.setImageURL("../assets/images/Dagr_Head.png");
		head.setRefinement(0);
		head.setQuality(RobotPartQuality.COMMON);
		
		Skill spreadCure = new Skill();
		spreadCure.setId(5L);
		spreadCure.setName("Spread Cure");
		spreadCure.setType(SkillType.LIGHT);
		spreadCure.setRange(SkillRange.ALL_TEAMATES);
		spreadCure.setAccuracy(100);
		spreadCure.getImpactOnMyself().setResistance(50);
		
		head.setSkill(spreadCure);
		dagr.getParts().add(head);
		
		RobotPart legs = new RobotPart();
		legs.setId(6L);
		legs.setType(RobotPartType.LEGS);
		legs.setImageURL("../assets/images/Dagr_Legs.png");
		legs.setRefinement(0);
		legs.setQuality(RobotPartQuality.COMMON);
		
		Skill spreadVirus = new Skill();
		spreadVirus.setId(6L);
		spreadVirus.setName("Spread Virus");
		spreadVirus.setType(SkillType.VIRUS);
		spreadVirus.setRange(SkillRange.ONE_ENEMY);
		spreadVirus.setAttackType(AttackType.PHYSICAL);
		spreadVirus.setAccuracy(90);
		spreadVirus.setBasePower(100);
		
		legs.setSkill(spreadVirus);
		dagr.getParts().add(legs);
		
		RobotPart rightArm = new RobotPart();
		rightArm.setId(7L);
		rightArm.setType(RobotPartType.RIGHT_ARM);
		rightArm.setImageURL("../assets/images/Dagr_Right_Arm.png");
		rightArm.setRefinement(0);
		rightArm.setQuality(RobotPartQuality.COMMON);
		
		Skill idunBless = new Skill();
		idunBless.setId(7L);
		idunBless.setName("Idun Bless");
		idunBless.setType(SkillType.LIGHT);
		idunBless.setRange(SkillRange.ONE_ENEMY);
		idunBless.setAttackType(AttackType.PHYSICAL);
		
		rightArm.setSkill(idunBless);
		dagr.getParts().add(rightArm);
		
		RobotPart leftArm = new RobotPart();
		leftArm.setId(8L);
		leftArm.setType(RobotPartType.LEFT_ARM);
		leftArm.setImageURL("../assets/images/Dagr_Left_Arm.png");
		leftArm.setRefinement(0);
		leftArm.setQuality(RobotPartQuality.COMMON);
		
		Skill lightExplosion = new Skill();
		lightExplosion.setId(8L);
		lightExplosion.setName("Light Explosion");
		lightExplosion.setType(SkillType.LIGHT);
		lightExplosion.setRange(SkillRange.ONE_ENEMY);
		lightExplosion.setAttackType(AttackType.PHYSICAL);
		lightExplosion.setAccuracy(100);
		lightExplosion.setBasePower(95);
		
		leftArm.setSkill(lightExplosion);
		dagr.getParts().add(leftArm);
		
		RobotPart chest = new RobotPart();
		chest.setId(9L);
		chest.setType(RobotPartType.CHEST);
		chest.setImageURL("../assets/images/Dagr_Chest.png");
		chest.setRefinement(0);
		chest.setQuality(RobotPartQuality.COMMON);
		
		Skill lightShield = new Skill();
		lightShield.setId(9L);
		lightShield.setName("Light Shield");
		lightShield.setType(SkillType.LIGHT);
		lightShield.setRange(SkillRange.ONE_ENEMY);
		lightShield.setAttackType(AttackType.BUFF);
		lightShield.setAccuracy(100);
		lightShield.setBasePower(100);
		
		chest.setSkill(lightShield);
		dagr.getParts().add(chest);
		
		return dagr;
	}	
	
	private static Robot createFenrir() {
		Robot fenrir = new Robot();

		fenrir.setId(2L);
		fenrir.setLevel(100);
		fenrir.setName("Fenrir");
		fenrir.setDescription("Fenrir");
		fenrir.setRobotClass(SkillType.BEAST);
		fenrir.setMainSkillType(SkillType.BALMIUM);
		fenrir.setImageIconURL("../assets/icons/Beowulf.png");
		fenrir.setImageURL("../assets/images/Fenrir.png");
		fenrir.getAttributes().setResistance(70);
		fenrir.getAttributes().setPhysicalAttack(130);
		fenrir.getAttributes().setPhysicalDefense(100);
		fenrir.getAttributes().setRunicAttack(55);
		fenrir.getAttributes().setRunicDefense(80);
		fenrir.getAttributes().setMobility(70);
		
		RobotPart head = new RobotPart();
		head.setId(10L);
		head.setType(RobotPartType.HEAD);
		head.setImageURL("../assets/images/Fenrir_Head.png");
		head.setRefinement(0);
		head.setQuality(RobotPartQuality.COMMON);
		
		Skill bite = new Skill();
		bite.setId(10L);
		bite.setName("Bite");
		bite.setRange(SkillRange.ONE_ENEMY);
		bite.setAccuracy(100);
		bite.getImpactOnMyself().setResistance(-10);
		bite.getImpactOnEnemy().setPhysicalAttack(30);
		bite.getImpactOnEnemy().setRunicAttack(30);
		
		head.setSkill(bite);
		fenrir.getParts().add(head);
		
		RobotPart legs = new RobotPart();
		legs.setId(11L);
		legs.setType(RobotPartType.LEGS);
		legs.setImageURL("../assets/images/Fenrir_Legs.png");
		legs.setRefinement(0);
		legs.setQuality(RobotPartQuality.COMMON);
		
		Skill aborbMinerals = new Skill();
		aborbMinerals.setId(11L);
		aborbMinerals.setName("Absorb Minerals");
		aborbMinerals.setType(SkillType.EARTH);
		aborbMinerals.setRange(SkillRange.ALL_PLAYERS);
		aborbMinerals.setAttackType(AttackType.PHYSICAL);
		aborbMinerals.setAccuracy(100);
		aborbMinerals.setBasePower(100);
		
		legs.setSkill(aborbMinerals);
		fenrir.getParts().add(legs);
		
		RobotPart rightArm = new RobotPart();
		rightArm.setId(12L);
		rightArm.setType(RobotPartType.RIGHT_ARM);
		rightArm.setImageURL("../assets/images/Fenrir_Right_Arm.png");
		rightArm.setRefinement(0);
		rightArm.setQuality(RobotPartQuality.COMMON);
		
		Skill releaseHandle = new Skill();
		releaseHandle.setId(12L);
		releaseHandle.setName("Release Handle");
		releaseHandle.setType(SkillType.LIGHT);
		releaseHandle.setRange(SkillRange.ONE_ENEMY);
		releaseHandle.setAttackType(AttackType.PHYSICAL);
		releaseHandle.setAccuracy(100);
		releaseHandle.setBasePower(80);
		releaseHandle.setCounterAttack(true);
		
		rightArm.setSkill(releaseHandle);
		fenrir.getParts().add(rightArm);
		
		RobotPart leftArm = new RobotPart();
		leftArm.setId(13L);
		leftArm.setType(RobotPartType.LEFT_ARM);
		leftArm.setImageURL("../assets/images/Fenrir_Left_Arm.png");
		leftArm.setRefinement(0);
		leftArm.setQuality(RobotPartQuality.COMMON);
		
		Skill sharpenClaws = new Skill();
		sharpenClaws.setId(13L);
		sharpenClaws.setName("Sharpen Claws");
		sharpenClaws.setType(SkillType.LIGHT);
		sharpenClaws.setRange(SkillRange.ONE_ENEMY);
		sharpenClaws.setAttackType(AttackType.PHYSICAL);
		sharpenClaws.setAccuracy(100);
		sharpenClaws.setBasePower(40);
		
		leftArm.setSkill(sharpenClaws);
		fenrir.getParts().add(leftArm);
		
		RobotPart chest = new RobotPart();
		chest.setId(14L);
		chest.setType(RobotPartType.CHEST);
		chest.setImageURL("../assets/images/Fenrir_Chest.png");
		chest.setRefinement(0);
		chest.setQuality(RobotPartQuality.COMMON);
		
		Skill idunBless = new Skill();
		idunBless.setId(14L);
		idunBless.setName("Idun Bless");
		idunBless.setType(SkillType.STONE);
		idunBless.setRange(SkillRange.ONE_ENEMY);
		idunBless.setAttackType(AttackType.PHYSICAL);
		idunBless.setAccuracy(80);
		idunBless.setBasePower(100);
		idunBless.setCriticalProbability(10);
		
		chest.setSkill(idunBless);
		fenrir.getParts().add(chest);
		
		return fenrir;
	}	
	
	private static void saveXML(Robot robot, String filename) throws Exception {
		JAXBContext jaxbContext = JAXBContext.newInstance(Robot.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(robot, new File(".", "src/" + BASE_PACKAGE + filename));
	}
	
	public static void main(String[] args) throws Exception {
//		saveXML(createBeowulf(), "beowulf.xml");
//		saveXML(createDagr(),    "dagr.xml");
//		saveXML(createFenrir(),  "fenrir.xml");
		
		Robot beowulf = createDagr();
		for (RobotPart part : beowulf.getParts()) {
			System.out.println(part.getType() + " " +  part.getOriginalDurability());
		}
	}
}
