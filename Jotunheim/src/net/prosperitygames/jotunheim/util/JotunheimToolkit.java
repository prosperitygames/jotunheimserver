package net.prosperitygames.jotunheim.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public class JotunheimToolkit {
	private static final String TEMPLATES_PACKAGE = "/net/prosperitygames/jotunheim/templates/";
	// That's the official regex for validating email addresses according to RFC 2822.
	private static final String EMAIL_REGEX = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	public static final Long TEAM_1 = 1L;
	public static final Long TEAM_2 = 2L;
	
	public static boolean isACorrectEmail(String email) {
		return Pattern.matches(EMAIL_REGEX, email);
	}
	
	public static String render(String templateName, Map<String, Object> model) throws IOException, TemplateException {
		InputStream input = JotunheimToolkit.class.getResourceAsStream(TEMPLATES_PACKAGE + templateName);
		InputStreamReader reader = new InputStreamReader(input);
		
		Configuration cfg = new Configuration();
		Template template = new Template(templateName, reader, cfg);
		
		cfg.setObjectWrapper(new DefaultObjectWrapper());
		
		StringWriter buffer = new StringWriter();
		template.process(model, buffer);
		buffer.flush();
		
		return buffer.toString();
	}
	
	public static byte[] getAttachment(String attachmentName) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		
		InputStream input = JotunheimToolkit.class.getResourceAsStream(TEMPLATES_PACKAGE + "attachments/" + attachmentName);
		byte[] buffer     = new byte[1024];
		int length        = 0;
		
		while ((length = input.read(buffer)) > 0) {
			output.write(buffer, 0, length);
		}
		
		output.flush();
		return output.toByteArray();
	}
	
	@SuppressWarnings("unchecked")
	public static JSONArray toJSONArray(String[] arrayOfStrings) {
		JSONArray jsonArray = new JSONArray();
		
		for (String str : arrayOfStrings) {
			jsonArray.add(str);
		}
		
		return jsonArray;
	}
	
	@SuppressWarnings("unchecked")
	public static JSONArray toJSONArray(Collection<?> collection) {
		JSONArray jsonArray = new JSONArray();
		
		if (collection != null) {
			for (Object obj : collection) {
				if (obj instanceof JSONable) {
					jsonArray.add(((JSONable)obj).toJSON());
				} else {
					jsonArray.add(obj.toString());
				}
			}
		}
		
		return jsonArray;
	}
}
