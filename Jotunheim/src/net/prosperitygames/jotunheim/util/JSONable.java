package net.prosperitygames.jotunheim.util;

import org.json.simple.JSONObject;

public interface JSONable {
	public JSONObject toJSON();
}
