package net.prosperitygames.jotunheim;

import org.json.simple.JSONObject;
import org.red5.server.api.IConnection;

public class Jogador {
	public String clientId;
	public String login;
	public int x;
	public int y;
	public int energia;
	public IConnection connection;
	
	public Jogador(String clientId, String login, IConnection connection) {
		this.clientId   = clientId;
		this.login      = login;
		this.connection = connection;
		this.x          = 0;
		this.y          = 0;
		this.energia    = 100;
	}

	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		
		obj.put("id", clientId);
		obj.put("nome", login);
		obj.put("x", x);
		obj.put("y", y);
		obj.put("energia", energia);
		
		return obj;
	}
	
	@Override
	public int hashCode() {
		return clientId.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Jogador other = (Jogador) obj;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		return true;
	}
}
