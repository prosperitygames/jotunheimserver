package net.prosperitygames.jotunheim.server;

import lombok.Getter;
import lombok.Setter;
import net.prosperitygames.entities.Fight;
import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.logic.events.FightListener;

import org.red5.server.api.IConnection;
import org.red5.server.api.service.ServiceUtils;

public class RemoteFightListener implements FightListener {
	private static final String FIGHT_STARTED = "fightStarted";
	
	@Getter @Setter
	private User user;
	@Getter @Setter
	private IConnection connection;
	
	public RemoteFightListener(User user, IConnection connection) {
		this.user       = user;
		this.connection = connection;
	}

	@Override
	public void fightStarted(Fight fight) {
		ServiceUtils.invokeOnConnection(connection, FIGHT_STARTED, new Object[]{fight.toJSON().toString()});
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RemoteFightListener other = (RemoteFightListener) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}
}
