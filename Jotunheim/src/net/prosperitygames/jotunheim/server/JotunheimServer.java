package net.prosperitygames.jotunheim.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import net.prosperitygames.entities.AttackType;
import net.prosperitygames.entities.Fight;
import net.prosperitygames.entities.Robot;
import net.prosperitygames.entities.User;
import net.prosperitygames.jotunheim.logic.context.GameContext;
import net.prosperitygames.jotunheim.logic.fight.FightManager;
import net.prosperitygames.jotunheim.logic.fight.FightManagerListener;
import net.prosperitygames.jotunheim.logic.login.Authentication;
import net.prosperitygames.jotunheim.logic.robots.RobotManager;

import org.json.simple.JSONArray;
import org.red5.server.adapter.ApplicationAdapter;
import org.red5.server.api.IConnection;
import org.red5.server.api.IScope;
import org.red5.server.api.service.ServiceUtils;

public class JotunheimServer extends ApplicationAdapter implements FightManagerListener {
	@Getter @Setter
	private Authentication authentication;
	@Getter @Setter
	private FightManager fightManager;
	@Getter @Setter
	private RobotManager robotManager;
	
	private Set<String> fightManagerListeners;
	private Map<String, IConnection> clientConnections;
	
	public JotunheimServer() {
		super();
		fightManagerListeners = new HashSet<String>();
		clientConnections     = new HashMap<String, IConnection>();
	}
	
	public void initialize() {
		fightManager.addFightManagerListener(this);
	}
	
    @Override
	public boolean connect(IConnection conn, IScope scope, Object[] params) {
    	String id = conn.getClient().getId();
    	
    	clientConnections.put(id, conn);
    	ServiceUtils.invokeOnConnection(conn, "setClientId", new Object[]{id});
    	
    	return true;
    }
    
    public String login(Object[] params) {
    	String clientId = (String)params[0];
    	String username = (String)params[1];
    	String password = (String)params[2];
    	
    	authentication.login(username, password, GameContext.getInstance(clientId));
    	
    	return username;
    }
    
    public void logout(Object[] params) {
    	String clientId = (String)params[0];
    	authentication.logout(clientId);    	
    }

    public void register(Object[] params) {
    	String name     = (String)params[1];
    	String email    = (String)params[2];
    	String username = (String)params[3];
    	String password = (String)params[4];
    	
    	authentication.register(name, email, username, password);
    }
    
    public Fight createFight(Object[] params) {
    	String clientId  = (String)params[0];
    	String fightName = (String)params[1];
    	
    	return fightManager.createFight(getCurrentUser(clientId).getLogin(), fightName);
    }
    
    @SuppressWarnings("unchecked")
	public String getAvailableFights(Object[] params) {
    	List<Fight> fights = fightManager.listAvailableFights();
    	
    	JSONArray array = new JSONArray();
    	for (Fight fight : fights) {
    		array.add(fight.toJSON());
    	}
    	
    	return array.toString();
    }
    
    public void registerForFight(Object[] params) {
    	String clientId = (String)params[0];
    	long fightId    = (Integer)params[1];
    	fightManager.registerForFight(getCurrentUser(clientId), fightId);
    }
    
    public void registerAttack(Object[] params) {
    	long fightId          = (Integer)params[1];
    	AttackType type       = AttackType.valueOf((String)params[2]);
    	String attackerId     = (String)params[3];
    	String defenderId     = (String)params[4];
    	String attackerPartId = (String)params[5];
    	String defenderPartId = (String)params[6];
    	fightManager.registerAttack(fightId, type, attackerId, defenderId, attackerPartId, defenderPartId);
    }
    
    public void addFightManagerListener(Object[] params) {
    	fightManagerListeners.add((String)params[0]);
    }

    public void removeFightManagerListener(Object[] params) {
    	fightManagerListeners.remove((String)params[0]);
    }
    
    @Override
	public void disconnect(IConnection conn, IScope scope) {
		super.disconnect(conn, scope);
    	authentication.logout(conn.getClient().getId());
		clientConnections.remove(conn.getClient().getId());
    }
    
    private User getCurrentUser(String clientId) {
    	return (User)GameContext.getInstance(clientId).getAttribute("user");
    }

	@Override
	public void fightCreated(Fight fight) {
		notifyFightChanged(fight, "fightCreated");
	}

	@Override
	public void fightRemoved(Fight fight) {
		notifyFightChanged(fight, "fightRemoved");
	}

	@Override
	public void fightStartedRemoveItFromAvailableList(Fight fight) {
		notifyFightChanged(fight, "fightStartedRemoveItFromAvailableList");
	}
	
	private void notifyFightChanged(Fight fight, String methodToInvoke) {
		for (String clientId : fightManagerListeners) {
			ServiceUtils.invokeOnConnection(clientConnections.get(clientId), methodToInvoke, new Object[]{fight.toJSON().toString()});
		}
	}
	
	@SuppressWarnings("unchecked")
	public void startBattle(Object[] params) {
		String clientId = (String)params[0];
		long fightId    = (Integer)params[1];
		Fight f = fightManager.findFightById(fightId);
		ArrayList<String> robots = (ArrayList<String>)params[2];
		fightManager.setRobots(getCurrentUser(clientId), fightId, robots);
		if (f.isComplete()) {
			f.start();
		}
	}

	public void addFightListener(Object[] params) {
    	String clientId = (String)params[0];
    	long fightId    = (Integer)params[1];
		Fight fight     = fightManager.findFightById(fightId);
		
		fight.addFightListener(new RemoteFightListener(getCurrentUser(clientId), clientConnections.get(clientId)));
	}
	
	public void removeFightListener(Object[] params) {
    	String clientId = (String)params[0];
    	long fightId    = (Integer)params[1];
		Fight fight     = fightManager.findFightById(fightId);
		
		fight.removeFightListener(new RemoteFightListener(getCurrentUser(clientId), clientConnections.get(clientId)));
	}
	
	public List<Robot> getRobots(Object[] params) {
		return robotManager.getRobots();
	}
}
