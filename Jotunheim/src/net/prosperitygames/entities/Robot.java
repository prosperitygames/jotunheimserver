package net.prosperitygames.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Entity @Data
@XmlRootElement
public class Robot implements Serializable {
	private static final long serialVersionUID = 4538745519543464382L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private int level;
	private String name;
	private String description;
	private String imageIconURL;
	private String imageURL;
	private List<RobotPart> parts;
	private FightAttributes attributes;
	private SkillType robotClass;
	private SkillType mainSkillType;
	private boolean broken;
	
	public Robot() {
		parts      = new ArrayList<RobotPart>();
		attributes = new FightAttributes();
	}
	
	public RobotPart getPart(RobotPartType type) {
		for (RobotPart part : parts) {
			if (part.getType() == type) {
				return part;
			}
		}
		
		return null;
	}
}
