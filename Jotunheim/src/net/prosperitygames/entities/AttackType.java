package net.prosperitygames.entities;

public enum AttackType {
	PHYSICAL("PHYSICAL"),
	RUNIC("RUNIC"),
	BUFF("BUFF");
	
	String type;
	
	AttackType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return type;
	}
}
