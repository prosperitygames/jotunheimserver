package net.prosperitygames.entities;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity @Data
public class User implements Serializable {
	private static final long serialVersionUID = 8104385016639376567L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String name;
    private String email;
    private Boolean loggedin;
    private String login;
    private String password;
    private Boolean confirmedEmail;
    private BigInteger currentServerid;
}
