package net.prosperitygames.entities;

import java.io.Serializable;

import lombok.Data;

@Data
public class FightAttributes implements Serializable {
	private static final long serialVersionUID = -8968918800769343531L;
	
	private double resistance;
	private double physicalAttack;
	private double physicalDefense;
	private double runicAttack;
	private double runicDefense;
	private double mobility;
}
