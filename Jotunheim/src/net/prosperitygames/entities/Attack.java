package net.prosperitygames.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import net.prosperitygames.jotunheim.util.JSONable;

import org.json.simple.JSONObject;

@Entity @Data
public class Attack implements Serializable, JSONable {
	private static final long serialVersionUID = 3973333134609839793L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String name;
	private String attackerId;
	private String defenderId;
	private AttackType type;
	private String defenderPartId;
	private String attackerPartId;

	public Attack(String attackerId, AttackType type) {
		this.attackerId = attackerId;
		this.type       = type;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o instanceof Attack) {
			return ((Attack)o).getId() == this.id;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		
		json.put("id", id);
		json.put("name", name);
		json.put("type", type.toString());
		json.put("attack", new String[]{attackerId, defenderId});
		
		return json;
	}
}
