package net.prosperitygames.entities;

public enum RoomStatus {
	AWAITING_USERS,
	STARTED,
	CANCELLED
}
