package net.prosperitygames.entities;

public enum RobotPartType {
	HEAD(0.59),
	CHEST(1.0),
	RIGHT_ARM(0.35),
	LEFT_ARM(0.49),
	LEGS(0.75);
	
	private double armorFactor;
	
	RobotPartType(double armorFactor) {
		this.armorFactor = armorFactor;
	}
	
	public double getArmorFactor() {
		return armorFactor;
	}
}
