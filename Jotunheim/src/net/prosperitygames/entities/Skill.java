package net.prosperitygames.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity @Data
public class Skill implements Serializable {
	private static final long serialVersionUID = -31792863717761880L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String name;
	private String description;
	private SkillType type;
	private SkillRange range;
	private double accuracy;
	private double basePower;
	private AttackType attackType;
	private boolean counterAttack;
	private int criticalProbability;
	private FightAttributes impactOnMyself = new FightAttributes();
	private FightAttributes impactOnEnemy  = new FightAttributes();
}
