package net.prosperitygames.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import lombok.Data;
import net.prosperitygames.jotunheim.logic.events.FightListener;
import net.prosperitygames.jotunheim.logic.fight.ThisUserDoesNotBelongsToThisFightException;
import net.prosperitygames.jotunheim.util.JSONable;
import net.prosperitygames.jotunheim.util.JotunheimToolkit;

import org.json.simple.JSONObject;

@Entity @Data
public class Fight implements Serializable, JSONable {
	private static final long serialVersionUID = 3973333134609839793L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String name;
	private User owner;
	private User opponent;
	private Collection<Attack> attacks;
	private Collection<String> team1Robots;
	private Collection<String> team2Robots;
	
	@Transient
	private transient List<FightListener> listeners;

	public Fight() {
		this(null);
	}
	
	public Fight(Long id) {
		this.id = id;
		team1Robots = new ArrayList<String>();
		team2Robots = new ArrayList<String>();
		listeners   = new ArrayList<FightListener>();
	}

	public void registerAttack(AttackType type, String attackerId, String defenderId, String attackerPartId, String defenderPartId) {
		Attack a = new Attack(attackerId, type);
		a.setDefenderId(defenderId);
		a.setAttackerPartId(attackerPartId);
		a.setDefenderPartId(defenderPartId);
	}
	
	public void registerBuff(String clientId, long fightId, String attackerId, String attackerPartId) {
		Attack a = new Attack(attackerId, AttackType.BUFF);
		a.setAttackerPartId(attackerPartId);
	}

	public void setRobots(User user, List<String> robots) {
		if (owner != null && owner.equals(user)) {
			team1Robots.clear();
			team1Robots.addAll(robots);
		} else if (opponent != null && opponent.equals(user)){
			team2Robots.clear();
			team2Robots.addAll(robots);
		} else {
			throw new ThisUserDoesNotBelongsToThisFightException();
		}
	}
	
	public boolean isComplete() {
		return this.team1Robots.size() == 3 && this.team2Robots.size() == 3;
	}
	
	public void start() {
		for (FightListener listener : listeners) {
			listener.fightStarted(this);
		}
	}
	
	public void addFightListener(FightListener listener) {
		this.listeners.add(listener);
	}
	
	public void removeFightListener(FightListener listener) {
		this.listeners.remove(listener);
	}
	
	public boolean equals(Object o) {
		if (o instanceof Fight) {
			return ((Fight)o).getId() == this.id;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSON() {
		JSONObject json = new JSONObject();
		
		json.put("id",          id);
		json.put("name",        name);
		json.put("owner",       owner.getLogin());
		json.put("opponent",    opponent == null ? null : opponent.getLogin());
		json.put("team1Robots", JotunheimToolkit.toJSONArray(team1Robots));
		json.put("team2Robots", JotunheimToolkit.toJSONArray(team2Robots));
		json.put("attacks",     JotunheimToolkit.toJSONArray(attacks));
		
		return json;
	}
}
