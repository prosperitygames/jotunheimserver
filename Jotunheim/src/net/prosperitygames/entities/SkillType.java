package net.prosperitygames.entities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public enum SkillType {
	BEAST("Beast"),
	SHADOW("Shadow"),
	RUNE_KNIGHT("Rune Knight"),
	ELECTRIC("Electric"),
	WARRIOR("Warrior"),
	FIRE("Fire"),
	WIND("Wind"),
	ETHERIUM("Etherium"),
	BIOMECHA("Biomecha"),
	WATER("Water"),
	BALMIUM("Balmium"),
	STONE("Stone"),
	LIGHT("Light"),
	VIRUS("Virus"),
	GUARDIAN("Guardian"),
	EARTH("Earth"),
	ICE("Ice"),
	CRYSTAL("Crystal"),
	ANTIVIRUS("Antivirus"),
	DIVINE("Divine"),
	JOTUNIUM("Jotunium");
	
	private String type;
	private Map<SkillType, Double> strengthMap;
	
	SkillType(String type) {
		this.type        = type;
		this.strengthMap = new HashMap<SkillType, Double>();
	}
	
	public double getAttackStrengthFactor(SkillType target) {
		if (strengthMap.isEmpty()) {
			synchronized (strengthMap) {
				try {
					populateStrengthMap();
				} catch (IOException e) {
					throw new RuntimeException("Could not read the skill types strength table");
				}
			}
		}
		
		if (strengthMap.containsKey(target)) {
			return strengthMap.get(target);
		}
		
		return 1.0;
	}
	
	public void addAttackStrengthFactory(SkillType target, double factor) {
		strengthMap.put(target, factor);
	}
	
	private void populateStrengthMap() throws IOException {
		SkillType[] skills = SkillType.values();
		InputStream inputStream = SkillType.class.getResourceAsStream("/net/prosperitygames/jotunheim/data/skill_types_table.csv");
		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		
		int row = 0;
		while ((line = reader.readLine()) != null) {
			String[] cells = line.split(";");
			for (int column = 0; column < cells.length; column++) {
				double factor = Double.parseDouble(cells[column]);
				skills[row].addAttackStrengthFactory(skills[column], factor);
			}
			
			row++;
		}
		
		reader.close();
	}

	@Override
	public String toString() {
		return type;
	}
	
	public static void main(String[] args) {
		for (SkillType source : SkillType.values()) {
			System.out.println("Attacker: " + source);
			for (SkillType target : SkillType.values()) {
				System.out.println("\t" + target + " - " + source.getAttackStrengthFactor(target) + "x");
			}
		}
	}
}
