package net.prosperitygames.entities;

public enum SkillRange {
	ONE_ENEMY,
	ALL_PLAYERS,
	ALL_TEAMATES
}
