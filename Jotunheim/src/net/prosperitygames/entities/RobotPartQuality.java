package net.prosperitygames.entities;

public enum RobotPartQuality {
	LEGENDARY(1.57),
	EPIC(1.37),
	RARE(1.17),
	UNUSUAL(1.0),
	COMMON(0.89);
	
	private double qualityFactor;
	
	RobotPartQuality(double qualityFactor) {
		this.qualityFactor = qualityFactor;
	}
	
	public double getQualityFactor() {
		return qualityFactor;
	}
}
