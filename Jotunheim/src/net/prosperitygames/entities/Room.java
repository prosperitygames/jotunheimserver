package net.prosperitygames.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity @Data
public class Room implements Serializable {
	private static final long serialVersionUID = 3286806446749539192L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private String name;
	private User owner;
	private int capacity;
	private RoomStatus status = RoomStatus.AWAITING_USERS;
	private List<User> joinedUsers = new ArrayList<User>();
}
