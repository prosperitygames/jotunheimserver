package net.prosperitygames.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity @Data
public class RobotPart implements Serializable {
	public static double REFINEMENT_TABLE[] = {0.35, 0.49, 0.49, 0.59, 0.59, 0.63, 0.63, 0.76, 0.76, 0.89, 1.0};
	
	private static final long serialVersionUID = -4685813226852201549L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	private RobotPartType type;
	private String imageURL;
	private Skill skill;
	private RobotPartQuality quality;
	private int refinement;
	private Integer durability;
	private boolean broken;
	
	public int getDurability() {
		if (durability == null) {
			durability = getOriginalDurability();
		}
		return durability;
	}
	
	public void setDurability(int durability) {
		this.durability = durability;
	}
	
	public int getOriginalDurability() {
		return 5 * (int)(23 * REFINEMENT_TABLE[refinement] * quality.getQualityFactor() * type.getArmorFactor());
	}
}
