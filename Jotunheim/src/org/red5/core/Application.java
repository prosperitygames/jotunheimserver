package org.red5.core;

/*
 * RED5 Open Source Flash Server - http://www.osflash.org/red5
 * 
 * Copyright (c) 2006-2008 by respective authors (see below). All rights reserved.
 * 
 * This library is free software; you can redistribute it and/or modify it under the 
 * terms of the GNU Lesser General Public License as published by the Free Software 
 * Foundation; either version 2.1 of the License, or (at your option) any later 
 * version. 
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY 
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
 * PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along 
 * with this library; if not, write to the Free Software Foundation, Inc., 
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
 */

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import net.prosperitygames.jotunheim.Jogador;

import org.json.simple.JSONArray;
import org.red5.server.adapter.ApplicationAdapter;
import org.red5.server.api.IConnection;
import org.red5.server.api.IScope;
import org.red5.server.api.service.ServiceUtils;
import org.red5.server.api.so.ISharedObject;

/**
 * Sample application that uses the client manager.
 * 
 * @author The Red5 Project (red5@osflash.org)
 */
public class Application extends ApplicationAdapter {
	private static int indiceSala = 1;
	private Set<Jogador> clientesAguardando = new HashSet<Jogador>();
	private Map<String, Set<Jogador>> mapaAtacantes = new HashMap<String, Set<Jogador>>(); 
	private Map<String, Jogador> clientesConectados = new HashMap<String, Jogador>();
	
    @Override
	public boolean connect(IConnection conn, IScope scope, Object[] params) {
    	String id = conn.getClient().getId();
    	ServiceUtils.invokeOnConnection(conn, "setClientId", new Object[]{id});
    	clientesConectados.put(id, new Jogador(id, null, conn));
    	
		return true;
	}

    @Override
	public void disconnect(IConnection conn, IScope scope) {
		super.disconnect(conn, scope);
		removerCliente(conn);
	}

	private void removerCliente(IConnection conn) {
		String id = conn.getClient().getId();
		Jogador chaveParaRemover = new Jogador(id, null, null);
		clientesAguardando.remove(chaveParaRemover);
		clientesConectados.remove(id);
	}
    
    public String login(Object[] params) {
    	String login = (String)params[0];
    	return login;
    }
    
	public void aguardarInicio(Object[] params) {
    	String clientId = params[0].toString();
    	String login    = params[1].toString();
    	Jogador jogador = clientesConectados.get(clientId);
    	jogador.login   = login;
    	
    	synchronized (clientesAguardando) {
	    	clientesAguardando.add(jogador);
	    	
	    	if (clientesAguardando.size() >= 2) {
	    		String sala = "dadosJogo_" + indiceSala++;
	    		
	    		createSharedObject(scope, sala, false);
	    		ISharedObject dadosJogo = getSharedObject(scope, sala);
	    		
	    		int x = 0;
	    		int y = 0;
	    		
	    		for (Jogador j : clientesAguardando) {
	    			j.x = x;
	    			j.y = y;
	    			
	    			x += 200;
	    			if (x > 800) {
	    				y += 200;
	    				x = 0;
	    			}
	    			
					ServiceUtils.invokeOnConnection(j.connection, "iniciarJogo", new Object[] {sala});
	    		}

	    		System.out.println("108: " + getJogadoresJSON(clientesAguardando).toString());
	    		dadosJogo.setAttribute("jogadores", getJogadoresJSON(clientesAguardando).toString());
		    	clientesAguardando.clear();
	    	}
    	}
    }
    
    public void atacar(Object[] params) {
    	String clientId = params[0].toString();
    	String salaId   = params[1].toString();
    	
    	Set<Jogador> atacantes = null;
    	
    	synchronized (mapaAtacantes) {
	    	if (mapaAtacantes.containsKey(salaId)) {
	    		atacantes = mapaAtacantes.get(salaId);
	    	} else {
	    		atacantes = new HashSet<Jogador>();
	    		mapaAtacantes.put(salaId, atacantes);
	    	}
    	}
    	
    	synchronized (atacantes) {
			atacantes.add(clientesConectados.get(clientId));
			if (atacantes.size() >= 2) {
				boolean fimDeJogo = false;
				
				for (Jogador jogador : atacantes) {
					jogador.energia -= (int)(Math.random() * (jogador.energia + 1));
					if (jogador.energia <= 0) fimDeJogo = true;
					ServiceUtils.invokeOnConnection(jogador.connection, "finalizarAtaque", new Object[] {});
				}
				
				ISharedObject dadosJogo = getSharedObject(scope, salaId);
				System.out.println("142: " + getJogadoresJSON(atacantes).toString());
				dadosJogo.setAttribute("jogadores", getJogadoresJSON(atacantes).toString());

				if (fimDeJogo) {
					for (Jogador jogador : atacantes) {
						if (jogador.energia <= 0) {
							ServiceUtils.invokeOnConnection(jogador.connection, "vocePerdeu", new Object[] {});
						} else {
							ServiceUtils.invokeOnConnection(jogador.connection, "voceGanhou", new Object[] {});
						}
					}
				}
				
				atacantes.clear();
			}
		}
    }
    
    public void speak(Object[] params) {
    	String clientId = params[0].toString();
    	String salaId   = params[1].toString();
    	String speach   = params[2].toString();
    	
    	Set<Jogador> atacantes = null;
    	
    	synchronized (mapaAtacantes) {
	    	if (mapaAtacantes.containsKey(salaId)) {
	    		atacantes = mapaAtacantes.get(salaId);
	    	} else {
	    		atacantes = new HashSet<Jogador>();
	    		mapaAtacantes.put(salaId, atacantes);
	    	}
    	}
    	
    	synchronized (atacantes) {
			atacantes.add(clientesConectados.get(clientId));
			if (atacantes.size() >= 2) {				
				for (Jogador jogador : atacantes) {
					ServiceUtils.invokeOnConnection(jogador.connection, "updateChat", new Object[] {clientId, speach});
				}
			}
		}
    }
    
    @SuppressWarnings("unchecked")
	private JSONArray getJogadoresJSON(Collection<Jogador> jogadores) {
		JSONArray jogadoresJSON = new JSONArray();
		
		for (Jogador jogador : jogadores) {
			jogadoresJSON.add(jogador.toJSON());
		}
    	
		return jogadoresJSON;
    }
}